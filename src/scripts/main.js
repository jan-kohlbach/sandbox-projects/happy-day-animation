(() => {
  const h1 = document.querySelector('.second');
  const days = ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'];
  const date = new Date();

  h1.innerHTML = days[date.getDay()];
})();
